let collection = [];

    let print = () => {
      return collection;
    };


    let enqueue = (element) => {
    
        collection.push(element);

        return collection;
    };


    let dequeue = (element) => {
        collection.shift(element);
        return collection;
    };

    let front = () => {
        return collection[0];
    };


    let size = () => {
        return collection.length;
    };


    let isEmpty = () => {
        return collection.length === 0;
    };


module.exports = {
  print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty,
};
